create table user_tags (
  id text,
  keyword_value	text,
  keyword_status text,
  pricing_type text,
  keyword_match_type text,
  destination_url text,
  PRIMARY KEY(id)
);
