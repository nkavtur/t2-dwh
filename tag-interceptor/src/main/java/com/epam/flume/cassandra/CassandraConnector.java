package com.epam.flume.cassandra;

import static java.lang.String.format;
import java.io.Closeable;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

/**
 * @author mikalai_kautur@epam.com
 */
@Component
public class CassandraConnector implements Closeable {
    private static final Logger LOG = Logger.getLogger(CassandraConnector.class);
    @Autowired private CassandraConfig config;

    private Cluster cluster;
    private Session session;

    @PostConstruct
    public void init() {
        cluster = Cluster.builder().addContactPoint(config.getHost()).addContactPoint(config.getPort()).build();
        LOG.info(format("Connected to cluster [%s]", cluster.getClusterName()));
        session = cluster.connect();
    }

    public Session getSession() {
        return session;
    }

    @Override
    public void close() throws IOException {
        if (cluster != null) {
            cluster.close();
        }
    }
}
