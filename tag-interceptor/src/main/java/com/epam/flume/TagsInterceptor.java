package com.epam.flume;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.apache.commons.lang.StringUtils.defaultIfEmpty;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.flume.cassandra.UserTagsRepository;

/**
 * Created by atomkevich on 4/25/16.
 * 
 * Joins user tags with raw events from kafka.
 */
public class TagsInterceptor implements Interceptor {
    public static final String TS_HEADER_NAME = "ts";
    public static final String COLLECTION_ITEMS_TERMINATED_BY = ",";
    private static final Logger LOG = Logger.getLogger(TagsInterceptor.class);
    private DateFormat parser = new SimpleDateFormat("yyyyMMddhhmmssSSS");
    private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    private ClassPathXmlApplicationContext context;
    private UserTagsRepository userTagsRepository;

    @Override
    public void initialize() {
        context = new ClassPathXmlApplicationContext("application-context.xml");
        userTagsRepository = context.getBean(UserTagsRepository.class);
    }

    @Override
    public Event intercept(Event event) {
        String body = new String(event.getBody());
        String[] splits = body.toString().split("\t");

        Map<String, String> headers = event.getHeaders();
        headers.put(TS_HEADER_NAME, convertToDate(splits[1]));

        String tagsId = splits[20];
        String tags = userTagsRepository.getUserTags(tagsId);
        event.setBody((format("%s\t%s", body, defaultIfEmpty(tags, EMPTY))).getBytes());

        return event;
    }

    private String convertToDate(String date) {
        try {
            return formatter.format(parser.parse(date));
        } catch (ParseException e) {
            LOG.error(format("Can't parse timestamp [%s]", date));
        }
        return EMPTY;
    }

    @Override
    public List<Event> intercept(List<Event> events) {
        return events.stream().map(e -> intercept(e)).collect(toList());
    }

    @Override
    public void close() {
        try {
            if (context != null) context.close();
            if (userTagsRepository != null) userTagsRepository.close();
        } catch (IOException e) {
            LOG.error(format("Error while closing backupRepository. Caused by: %s", e.getCause()));
        }
    }

    public static class Builder implements Interceptor.Builder {
        @Override
        public Interceptor build() {
            return new TagsInterceptor();
        }

        @Override
        public void configure(Context context) {}
    }
}
