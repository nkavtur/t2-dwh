1) Download latest flume and deflate to some directory
2) In $FLUME_HOME/bin/flume-ng add JAVA_OPTS="-xms100m -xmx256m" (Or whatever amount of memory works for you)
3) Build tag-interceptor with 'mvn clean package'
4) Put tag-interceptor/target/tag-interceptor-1.0.0.jar to $FLUME_HOME/lib
5) Run run.sh (update path to FLUME_HOME if needed)
