package com.epam.user_tags_populate;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

public class App {
    private static final String INSERT = "INSERT INTO team1.user_tags(id, keyword_value, keyword_status, pricing_type, keyword_match_type, destination_url) VALUES (?, ?, ?, ?, ?, ?)";

    public static void main(String[] args) throws IOException {
        Cluster cluster = null;
        try {
            cluster = Cluster.builder().addContactPoint("127.0.0.1").addContactPoint("9042").build();
            Session session = cluster.newSession();

            try (BufferedReader reader = new BufferedReader(new FileReader("user.profile.tags.us.txt.out"))) {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    String[] splits = line.split("\\t");
                    String id = splits[0];
                    String value = splits[1];
                    String status = splits[2];
                    String pricingType = splits[3];
                    String matchType = splits[4];
                    String url = splits[5];

                    session.execute(INSERT, id, value, status, pricingType, matchType, url);
                }
            }
        } finally {
            cluster.close();
        }
    }
}
